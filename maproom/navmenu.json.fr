{
"@context": {
"links": { "@container": "@list" },
"groups": {}
},
"options": [
{"href": "http://www.anacim.sn/",
 "title": "ANACIM - Accueil"}
],
"groups": [
{
"title": "ANACIM",
"links": [
{"href": "/",
 "title": "Datathèque"}
]
},
{
"title": "Datathèque",
"links": [
{"href": "/maproom/",
 "title": "Map Rooms"},
{"href": "/docfind/facetedbrowser.html?taxa=iridl%3ADataset_Search&itemClass=iridl%3Adataset",
 "title": "Bases de Données"},
{"href": "/dochelp/Documentation/funcindex.html",
 "title": "Fonctions"},
{"href": "/dochelp/Tutorial/",
 "title": "Tutoriel"},
{"href": "/dochelp/StatTutorial/",
"title": "Tutoriel - Statistiques"},
{"href": "/dochelp/QA/",
"title": "FAQ"},
{"href": "/expert/",
 "title": "Mode Expert"}
]
}
]
}

